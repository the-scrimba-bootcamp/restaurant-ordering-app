import { menuArray } from "./data/data";

const mainHtml = document.querySelector("#main-content");
const currentOrderHtml = document.querySelector("#current-order");
const checkoutBtn = document.querySelector("#checkout");
const paymentDialog = document.querySelector("#payment");

let totalPrice = 0;
let totalItems = 0;
// let itemsInCart = [];
let itemsInCart = new Set();

// If the user is adding an item to the cart:
document.addEventListener("click", (e) => {
  if (e.target.dataset.productId) {
    manageCart("add", e.target.dataset.productId);
  }

  if (e.target.dataset.incartPid) {
    manageCart("remove", e.target.dataset.incartPid);
  }
});

const getProducts = () => {
  for (const product of menuArray) {
    createProductHtml(
      product.emoji,
      product.name,
      product.ingredients,
      product.price,
      product.id
    );
  }
};

const createProductHtml = (
  productEmoji,
  productName,
  productIngredients,
  productPrice,
  productId
) => {
  let builtHtml = ` 
  <div class="product">
        <div class="product-emoji">${productEmoji}</div>
        <div class="product-name">${productName}</div>
        <div class="product-ingredients">${productIngredients}</div>
        <div class="product-price">$${productPrice}</div>
        <button data-product-id="${productId}" class="product-add">+</button>
  </div>
  `;
  render(mainHtml, builtHtml);
};

const manageCart = (action, prodId) => {
  if (action === "add") {
    if (!itemsInCart.has(prodId)) {
      itemsInCart.add(prodId);
      totalPrice += menuArray[prodId].price;
      console.log(itemsInCart);
      handleOrderHtml();
    }
  }

  if (action === "remove") {
    if (itemsInCart.has(prodId)) {
      itemsInCart.delete(prodId);
      if (totalPrice > 0) {
        totalPrice -= menuArray[prodId].price;
      }
      handleOrderHtml();
    }
  }

  if (action === "removeAll") {
    if (itemsInCart.size > 0) {
      itemsInCart.clear();
      if (totalPrice > 0) {
        totalPrice = 0;
      }
      handleOrderHtml();
    }
  }
};

const handleOrderHtml = () => {
  {
    currentOrderHtml.innerHTML = "";
    let orderHtml = " ";
    for (let item of itemsInCart) {
      orderHtml += ` 
    <div class="ordered-item">
      <p>${menuArray[item].name}</p>
      <button data-incart-pid=${Number(
        menuArray[item].id
      )} class="remove-item-btn" type="button">remove</button>
      <p>$${menuArray[item].price}</p>
    </div>
  `;
    }

    let orderBottom = `  
      <h3>Your Order</h3>
      ${orderHtml}
      <div class="total">
        <p>Total price</p>
        <p>$${totalPrice}</p>
      </div>
      <button id="checkout">Complete Order</button>
`;
    if (itemsInCart.size > 0) {
      return render(currentOrderHtml, orderBottom);
    }
  }
};

const handleModal = (targetModal, action) => {
  if (action === "show") {
    // TODO: Handle displaying the modal on the screen.
    targetModal.showModal();
  }

  if (action === "hide") {
    targetModal.close();
  }
};

const resetCartState = () => {
  // itemsInCart = new Set();
  manageCart("removeAll", "");
};

const completeOrder = () => {
  handleModal(paymentDialog, "hide");
  // TODO: Show HTML message telling the order is complete.

  // Reset the cart:
  resetCartState();
};

const render = (target, source) => {
  target.innerHTML += source;
};
document.addEventListener("load", getProducts());
document.addEventListener("click", (e) => {
  if (e.target && e.target.id === "checkout") {
    paymentDialog.showModal();
  }

  if (e.target && e.target.id === "submit-order") {
    e.preventDefault();
    completeOrder();
  }
});
